let cities = [{
        name: "Benbrook, TX",
        latitude: 32.6732,
        longitude: -97.4606
    },
    {
        name: "Detroit, MI",
        latitude: 42.3384,
        longitude: -83.0813
    },
    {
        name: "New Orleans, LA",
        latitude: 30.0065,
        longitude: -90.0811
    },
    {
        name: "Juneau, AK",
        latitude: 58.3327,
        longitude: -134.4447
    },
    {
        name: "El Paso, TX",
        latitude: 31.7690,
        longitude: -106.4547
    }
];


window.onload = function () {
    const nightBtn = document.getElementById("nightBtn");
    const dayBtn = document.getElementById("dayBtn");
    const allBtn = document.getElementById("allBtn");
    const clearBtn = document.getElementById("clearBtn");

    nightBtn.onclick = getNightForecasts;
    dayBtn.onclick = getDayForecasts;
    allBtn.onclick = getAllCityForecasts;
    clearBtn.onclick = clearValues;

    loadCitiesDropdown();
};

function loadCitiesDropdown() {
    const citiesList = document.getElementById("citiesList");
    let citiesListLength = cities.length;

    let selectOption = new Option("Select city...", "");;
    selectOption.value = "";
    selectOption.onclick = clearValues;
    citiesList.appendChild(selectOption);

    for (let i = 0; i < citiesListLength; i++) {
        let cityOption = new Option(cities[i].name, cities[i].name);
        cityOption.onclick = getAllCityForecasts;
        citiesList.appendChild(cityOption);
    }

}

function getAllCityForecasts() {
    getForecastData("all");
}

function getDayForecasts() {
    getForecastData("day");
}

function getNightForecasts() {
    getForecastData("night");
}

function getForecastData(dayOrNight) {
    const citiesList = document.getElementById("citiesList");
    const selectedIndex = citiesList.selectedIndex;

    let citiesLength = cities.length;

    for (let i = 0; i < citiesLength; i++) {
        if (citiesList[selectedIndex].value === cities[i].name) {
            getStationURL(cities[i], dayOrNight);
        }
    }

}

function clearValues() {
    const tbody = document.getElementById("forecastTblBody");
    tbody.innerHTML = "";
}

function getStationURL(selectedCity, dayOrNight) {

    let stationLookupUrl =
        `https://api.weather.gov/points/${selectedCity.latitude},${selectedCity.longitude}`;
    console.log(stationLookupUrl);
    clearValues();
    fetch(stationLookupUrl)
        .then(response => response.json())
        .then(jsonData => {
            let weatherUrl = jsonData.properties.forecast;
            getWeather(weatherUrl, dayOrNight);
        })
        .catch(err => console.error(err));
}

function getWeather(weatherURL, dayOrNight) {

    fetch(weatherURL)
        .then(response => response.json())
        .then(jsonData => {
            let forecastArray = jsonData.properties.periods;
            let forecastData = [];

            switch (dayOrNight) {
                case "all":
                    forecastData = forecastArray;
                    break;
                case "day":
                    forecastData = forecastArray.filter(function (forecast) {
                        return forecast.isDaytime == true;
                    });
                    break;
                case "night":
                    forecastData = forecastArray.filter(function (forecast) {
                        return forecast.isDaytime == false;
                    });
                    break;
                default:
                    forecastData = forecastArray;
            }

            for (let i = 0; i < forecastArray.length; i++) {
                displayWeather(forecastData[i]);
            }
        })
        .catch(err => console.error(err));
}

function displayWeather(forecast) {
    const forecastTblBody = document.getElementById("forecastTblBody");
    if (forecastTblBody) {
        let row = forecastTblBody.insertRow(-1);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);
        let cell4 = row.insertCell(3);
        let cell5 = row.insertCell(4);
        let cell6 = row.insertCell(5);
        let cell7 = row.insertCell(6);
        let cell8 = row.insertCell(7);
        let cell9 = row.insertCell(8);
        let cell10 = row.insertCell(9);
        let cell11 = row.insertCell(10);

        cell1.innerHTML = "<img src='" + forecast.icon + "'>";
        cell2.innerHTML = forecast.name;
        cell3.innerHTML = new Date(forecast.startTime);
        cell4.innerHTML = new Date(forecast.endTime);
        cell5.innerHTML = forecast.shortForecast;
        cell6.innerHTML = forecast.temperature + " " + forecast.temperatureUnit;
        cell7.innerHTML = forecast.windSpeed + " " + forecast.windDirection;
        cell8.innerHTML = forecast.probabilityOfPrecipitation.value + " " + forecast.probabilityOfPrecipitation.unitCode;
        cell9.innerHTML = (forecast.dewpoint.value).toFixed(2); + " " + forecast.dewpoint.unitCode;
        cell10.innerHTML = forecast.relativeHumidity.value + " " + forecast.relativeHumidity.unitCode;
        cell11.innerHTML = forecast.detailedForecast;
    }
}